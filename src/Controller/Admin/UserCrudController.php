<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCrudController extends AbstractCrudController
{

  /** @var UserPasswordEncoderInterface */
private $encoder;

  /**
   * UserCrudController constructor.
   * @param UserPasswordEncoderInterface $encoder
   */
  public function __construct(UserPasswordEncoderInterface $encoder)
  {
    $this->encoder = $encoder;
  }


  public static function getEntityFqcn(): string
    {
        return User::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('email'),
            TextField::new('name'),
            TextField::new('address'),
            TextField::new('plainPassword'),
            BooleanField::new('admin'),
        ];
    }

  public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
  {
    /**  @var User $entityInstance */
    $entityInstance->setPassword($this->encoder->encodePassword($entityInstance, $entityInstance->getPlainPassword()));
    parent::persistEntity($entityManager, $entityInstance);
  }

  public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
  {
    /**  @var User $entityInstance */
    $entityInstance->setPassword($this->encoder->encodePassword($entityInstance, $entityInstance->getPlainPassword()));
    parent::persistEntity($entityManager, $entityInstance);
  }

}
