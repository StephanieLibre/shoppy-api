<?php

namespace App\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\OrderLine;
use App\Entity\Product;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Service\CartManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Validator\Constraints;

class ProductController extends AbstractController
{

  /**
   * @Route("/api/products", name="list_product", methods={"GET"})
   */
  public function index(ProductRepository $repository)
  {
    $products = $repository->findAll();
    return $this->json($products, 200, [], ["groups" => "product.list"]);
  }


  /**
   * @Route("/api/products/{product}", name="show_product", methods={"GET"})
   */
  public function show(Product $product)
  {
    return $this->json($product, 200, [], ["groups" => "product.show"]);
  }
}
